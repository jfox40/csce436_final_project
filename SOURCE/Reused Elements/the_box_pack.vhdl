--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

package theBoxParts is


--/////////////////////// Audio Codec Wrapper //////////////////////////////////--

component Audio_Codec_Wrapper 
    Port ( clk : in STD_LOGIC;
        reset_n : in STD_LOGIC;
        ac_mclk : out STD_LOGIC;
        ac_adc_sdata : in STD_LOGIC;
        ac_dac_sdata : out STD_LOGIC;
        ac_bclk : out STD_LOGIC;
        ac_lrclk : out STD_LOGIC;
        ready : out STD_LOGIC;
        L_bus_in : in std_logic_vector(17 downto 0); -- left channel input to DAC
        R_bus_in : in std_logic_vector(17 downto 0); -- right channel input to DAC
        L_bus_out : out  std_logic_vector(17 downto 0); -- left channel output from ADC
        R_bus_out : out  std_logic_vector(17 downto 0); -- right channel output from ADC
        scl : inout STD_LOGIC;
        sda : inout STD_LOGIC);
end component;

component Settings_cu is
  Port (clk: in std_logic;
        reset_n: in std_logic;
        settingData: in std_logic_vector(7 downto 0);
        cw: out std_logic_vector(7 downto 0));
end component;

component The_Box_dp is
  Port (clk: in std_logic;
        reset_n: in std_logic;
        ac_mclk : out STD_LOGIC;
		ac_adc_sdata : in STD_LOGIC;
		ac_dac_sdata : out STD_LOGIC;
		ac_bclk : out STD_LOGIC;
		ac_lrclk : out STD_LOGIC;
        scl : inout STD_LOGIC;
        sda : inout STD_LOGIC;
		tmds : out  STD_LOGIC_VECTOR (3 downto 0);
        tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
        lg_cw: in std_logic_vector(2 downto 0);
        c_cw: in std_logic_vector(2 downto 0);
        set_cw: in std_logic_vector(7 downto 0);
		shotTracker: std_logic_vector(14 downto 0);
        shotsTaken: unsigned(3 downto 0));
end component;

component Cube_cu is
  Port (clk: in std_logic;
        reset_n: in std_logic;
        controlInput: in std_logic_vector(4 downto 0);
        cw: out std_logic_vector(2 downto 0));
end component;

component Lightgun_cu is
  Port (clk, reset_n: in std_logic;
        playerSelect: in std_logic;
        gunData: in std_logic_vector(2 downto 0);
        P1Hit, P1Miss: out std_logic_vector(3 downto 0);
        cw: out std_logic_vector(2 downto 0);
		shotTracker: out std_logic_vector(14 downto 0);
        shotsTaken: out unsigned(3 downto 0);
		gunClock: in std_logic;
		displayReady: out std_logic;
		matchLength: in std_logic_vector(1 downto 0));
end component;

component Audio_Control is
  Port (clk: in std_logic;
        reset_n: in std_logic;
        ac_mclk : out STD_LOGIC;
		ac_adc_sdata : in STD_LOGIC;
		ac_dac_sdata : out STD_LOGIC;
		ac_bclk : out STD_LOGIC;
		ac_lrclk : out STD_LOGIC;
        scl : inout STD_LOGIC;
        sda : inout STD_LOGIC;
        effectSelection: in std_logic_vector(1 downto 0));
end component;

component DisplayManager is
  Port (clk: in std_logic;
        reset_n: in std_logic;
        tmds: out std_logic_vector(3 downto 0);
        tmdsb: out std_logic_vector(3 downto 0);
        lg_cw: in std_logic_vector(2 downto 0);
        c_cw: in std_logic_vector(2 downto 0);
        set_cw: in std_logic_vector(7 downto 0);
		shotTracker: std_logic_vector(14 downto 0);
        shotsTaken: unsigned(3 downto 0));
end component;

component video is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
		   settings: std_logic_vector(7 downto 0);
           shotTracker: std_logic_vector(14 downto 0);
           shotsTaken: unsigned(3 downto 0);
           control: in std_logic_vector(4 downto 0));
end component;

component Comparator is
	generic (N: integer := 10);
	port(
		D0, D1: in std_logic_vector (N-1 downto 0);
		L, E, G: out std_logic);
end component;

component Mux2to1 is
	generic (N: integer := 10);
	port(
		D0, D1: in std_logic_vector (N-1 downto 0);
		selection: in std_logic;
		Q: out std_logic_vector (N-1 downto 0));
end component;

component reg is
	generic (N: integer := 18);
	port(
	    clk, reset: in std_logic;
		D: in std_logic_vector(N-1 downto 0);
		Q: out std_logic_vector (N-1 downto 0));
end component;

component NESControllerRead is
  Port (reset_n: in std_logic;
        dataIn: in std_logic;
        dataOut: out std_logic_vector(7 downto 0);
		nesClk: in std_logic;
		latch: out std_logic);
end component;

component AddressCounter is
  Port (clk, reset: in std_logic;
        cw: in std_logic_vector(1 downto 0);
        count: out std_logic_vector(9 downto 0));
end component;

end theBoxParts;
