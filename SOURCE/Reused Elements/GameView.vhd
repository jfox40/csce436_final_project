---------------------------------------------------------------------------------
-- Name:	Jacob Fox
-- Date:	Spring 2020
-- Course: 	CSCE 436
-- File: 	GameView.vhdl
-- HW:		Final Project 
-- Purp:	Displays main game screen 
--
-- Doc:	Basic design came from lecture files
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity GameScreen is
    Port ( row : in  unsigned(9 downto 0);
           column : in  unsigned(9 downto 0);
           r : out  std_logic_vector(7 downto 0);
           g : out  std_logic_vector(7 downto 0);
           b : out  std_logic_vector(7 downto 0);
           settings: std_logic_vector(7 downto 0);
           moveCube: std_logic;
           shotTracker: std_logic_vector(14 downto 0);
           shotsTaken: unsigned(3 downto 0);
           control: in std_logic_vector(4 downto 0));
end GameScreen;

architecture Behavioral of GameScreen is

signal edge, cube, flash, black, whiteHit, stopMove: std_logic;
signal hitMarker, moveControl, shotNumber: std_logic_vector(1 downto 0);
signal speed: unsigned(2 downto 0);
signal hitSize: unsigned(5 downto 0);
signal positionX: unsigned(9 downto 0) := "0101000000";
signal positionY: unsigned(9 downto 0) := "0011010111";
signal negativeHitboxX, negativeHitboxY: unsigned(9 downto 0);

begin     

--Control Word assignment

--Switches 2 and 3 control hitbox size
hitSize <= "001111" when settings(3 downto 2) = "00" else
           "010000" when settings(3 downto 2) = "01" else
           "100000" when settings(3 downto 2) = "10" else "111111"; 
          
--Switches 0 and 1 control speed 
speed <= "010"  when settings(1 downto 0) = "00" else
         "100"  when settings(1 downto 0) = "01" else
         "110"  when settings(1 downto 0) = "10" else "111";

--Switches 4 and 5 control number of shots in a round
shotNumber <= settings(5 downto 4);

--Assign control data to signals 
stopMove <= control(4);
moveControl <= control(3 downto 2);
flash <= control(1);
black <= control(0);

--Moves the cube at the flashing speed of clock using the cubes last direction
process(moveCube)
    begin 
        if(rising_Edge(moveCube)) then
            if(stopMove = '1' or flash = '1') then
                positionX <= positionX;
                positionY <= positionY;
            elsif(moveControl = "00" and positionX < 630-10) then 
                positionX <= positionX + (speed);
            elsif(moveControl = "01" and positionX > 10+10) then 
                positionX <= positionX - (speed);
            elsif(moveControl = "10" and positionY < 420-10) then
                positionY <= positionY + (speed); 
            elsif(moveControl = "11" and positionY > 10+10) then
                positionY <= positionY - (speed); 
            end if; 
        end if;
    end process; 

--Draw the play area
edge <= '1' when ((column = 10 or column = 420) and (row > 10 and row < 630)) or ((row = 10 or row = 630) and (column > 10 and column < 420))  else '0';

--Draw cube on screen 
cube <= '1' when ((row < positionX + 10) and (row > positionX - 10) and (column < positionY + 10) and (column > positionY - 10) and (flash = '0') and (settings(7 downto 6) /= "11")) else '0';  

negativeHitBoxX <= (positionX - (hitSize)) when positionX > (hitSize) else "0000000000"; 
negativeHitBoxY <= (positionY - (hitSize)) when positionY > (hitSize) else "0000000000";
--Flash white around cube when requested
whiteHit <= '1' when ((row < positionX + (hitSize)) and (row > negativeHitBoxX) and (column < positionY + (hitSize)) and (column > negativeHitBoxY) and (flash = '1')) else '0';

--Keep Score at bottom of screen 
hitMarker <= "01" when (row > 15 and row < 25 and column > 430  and column < 460 and shotsTaken = 0) else
         "10" when (row > 15 and row < 25 and column > 430  and column < 460 and shotsTaken > 0 and shotTracker(0) = '0') else 
         "11" when (row > 15 and row < 25 and column > 430  and column < 460 and shotsTaken > 0 and shotTracker(0) = '1') else 
         "01" when (row > 35 and row < 45 and column > 430  and column < 460 and shotsTaken <= 1) else 
         "10" when (row > 35 and row < 45 and column > 430  and column < 460 and shotsTaken > 1 and shotTracker(1) = '0') else 
         "11" when (row > 35 and row < 45 and column > 430  and column < 460 and shotsTaken > 1 and shotTracker(1) = '1') else 
         "01" when (row > 55 and row < 65 and column > 430  and column < 460 and shotsTaken <= 2) else 
         "10" when (row > 55 and row < 65 and column > 430  and column < 460 and shotsTaken > 2 and shotTracker(2) = '0') else 
         "11" when (row > 55 and row < 65 and column > 430  and column < 460 and shotsTaken > 2 and shotTracker(2) = '1') else 
         "01" when (row > 75 and row < 85 and column > 430  and column < 460 and shotsTaken <= 3) else 
         "10" when (row > 75 and row < 85 and column > 430  and column < 460 and shotsTaken > 3 and shotTracker(3) = '0') else 
         "11" when (row > 75 and row < 85 and column > 430  and column < 460 and shotsTaken > 3 and shotTracker(3) = '1') else 
         "01" when (row > 95 and row < 105 and column > 430  and column < 460 and shotsTaken <= 4) else 
         "10" when (row > 95 and row < 105 and column > 430  and column < 460 and shotsTaken > 4 and shotTracker(4) = '0') else 
         "11" when (row > 95 and row < 105 and column > 430  and column < 460 and shotsTaken > 4 and shotTracker(4) = '1') else  
               
         "01" when (row > 115 and row < 125 and column > 430  and column < 460 and shotsTaken <= 5 and shotNumber(0) = '1') else
         "10" when (row > 115 and row < 125 and column > 430  and column < 460 and shotsTaken > 5 and shotTracker(5) = '0' and shotNumber(0) = '1') else 
         "11" when (row > 115 and row < 125 and column > 430  and column < 460 and shotsTaken > 5 and shotTracker(5) = '1' and shotNumber(0) = '1') else 
         "01" when (row > 135 and row < 145 and column > 430  and column < 460 and shotsTaken <= 6 and shotNumber(0) = '1') else 
         "10" when (row > 135 and row < 145 and column > 430  and column < 460 and shotsTaken > 6 and shotTracker(6) = '0' and shotNumber(0) = '1') else 
         "11" when (row > 135 and row < 145 and column > 430  and column < 460 and shotsTaken > 6 and shotTracker(6) = '1' and shotNumber(0) = '1') else 
         "01" when (row > 155 and row < 165 and column > 430  and column < 460 and shotsTaken <= 7 and shotNumber(0) = '1') else 
         "10" when (row > 155 and row < 165 and column > 430  and column < 460 and shotsTaken > 7 and shotTracker(7) = '0' and shotNumber(0) = '1') else 
         "11" when (row > 155 and row < 165 and column > 430  and column < 460 and shotsTaken > 7 and shotTracker(7) = '1' and shotNumber(0) = '1') else 
         "01" when (row > 175 and row < 185 and column > 430  and column < 460 and shotsTaken <= 8 and shotNumber(0) = '1') else 
         "10" when (row > 175 and row < 185 and column > 430  and column < 460 and shotsTaken > 8 and shotTracker(8) = '0' and shotNumber(0) = '1') else 
         "11" when (row > 175 and row < 185 and column > 430  and column < 460 and shotsTaken > 8 and shotTracker(8) = '1' and shotNumber(0) = '1') else 
         "01" when (row > 195 and row < 205 and column > 430  and column < 460 and shotsTaken <= 9 and shotNumber(0) = '1') else 
         "10" when (row > 195 and row < 205 and column > 430  and column < 460 and shotsTaken > 9 and shotTracker(9) = '0' and shotNumber(0) = '1') else 
         "11" when (row > 195 and row < 205 and column > 430  and column < 460 and shotsTaken > 9 and shotTracker(9) = '1' and shotNumber(0) = '1') else 
         
         "01" when (row > 215 and row < 225 and column > 430  and column < 460 and shotsTaken <= 10 and shotNumber = "11") else
         "10" when (row > 215 and row < 225 and column > 430  and column < 460 and shotsTaken > 10 and shotTracker(10) = '0' and shotNumber = "11") else 
         "11" when (row > 215 and row < 225 and column > 430  and column < 460 and shotsTaken > 10 and shotTracker(10) = '1' and shotNumber = "11") else 
         "01" when (row > 235 and row < 245 and column > 430  and column < 460 and shotsTaken <= 11 and shotNumber = "11") else 
         "10" when (row > 235 and row < 245 and column > 430  and column < 460 and shotsTaken > 11 and shotTracker(11) = '0' and shotNumber = "11") else 
         "11" when (row > 235 and row < 245 and column > 430  and column < 460 and shotsTaken > 11 and shotTracker(11) = '1' and shotNumber = "11") else 
         "01" when (row > 255 and row < 265 and column > 430  and column < 460 and shotsTaken <= 12 and shotNumber = "11") else 
         "10" when (row > 255 and row < 265 and column > 430  and column < 460 and shotsTaken > 12 and shotTracker(12) = '0' and shotNumber = "11") else 
         "11" when (row > 255 and row < 265 and column > 430  and column < 460 and shotsTaken > 12 and shotTracker(12) = '1' and shotNumber = "11") else 
         "01" when (row > 275 and row < 285 and column > 430  and column < 460 and shotsTaken <= 13 and shotNumber = "11") else 
         "10" when (row > 275 and row < 285 and column > 430  and column < 460 and shotsTaken > 13 and shotTracker(13) = '0' and shotNumber = "11") else 
         "11" when (row > 275 and row < 285 and column > 430  and column < 460 and shotsTaken > 13 and shotTracker(13) = '1' and shotNumber = "11") else 
         "01" when (row > 295 and row < 305 and column > 430  and column < 460 and shotsTaken <= 14 and shotNumber = "11") else 
         "10" when (row > 295 and row < 305 and column > 430  and column < 460 and shotsTaken > 14 and shotTracker(14) = '0' and shotNumber = "11") else 
         "11" when (row > 295 and row < 305 and column > 430  and column < 460 and shotsTaken > 14 and shotTracker(14) = '1' and shotNumber = "11") else "00";

--Send data to display
r <= x"FF" when (edge = '1' or whiteHit = '1' or hitMarker = "01" or hitMarker = "10") and cube = '0' and black = '0' else x"00";
g <= x"FF" when (edge = '1' or whiteHit = '1' or cube = '1' or hitMarker = "01" or hitMarker = "11") and black = '0' else x"00";
b <= x"FF" when (edge = '1' or whiteHit = '1' or cube = '1' or hitMarker = "01") and black = '0' else x"00"; 


end Behavioral;
