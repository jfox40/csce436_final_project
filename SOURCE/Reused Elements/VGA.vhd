---------------------------------------------------------------------------------
-- Name:	Jacob Fox
-- Date:	Spring 2020
-- Course: 	CSCE 436
-- File: 	VGA.vhdl
-- HW:		Final Project 
-- Purp:	Creates VGA component to generate vga signal, links to the gamescreen
--
-- Doc:	Basic design came from lecture files
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity VGA is
    Port(	clk: in  STD_LOGIC;
			reset_n : in  STD_LOGIC;
			h_sync : out  STD_LOGIC;
			v_sync : out  STD_LOGIC; 
			blank : out  STD_LOGIC;
			r: out STD_LOGIC_VECTOR(7 downto 0);
			g: out STD_LOGIC_VECTOR(7 downto 0);
			b: out STD_LOGIC_VECTOR(7 downto 0);
			stopMove: std_logic;
            settings: std_logic_vector(7 downto 0);
            shotTracker: std_logic_vector(14 downto 0);
            shotsTaken: unsigned(3 downto 0);
            control: in std_logic_vector(4 downto 0));
end VGA;

architecture structure of VGA is

component counter is
    	port(
    	    clk, reset, ctrl: in  STD_LOGIC;
			R: out std_logic;
			Q: out unsigned (9 downto 0)
			);
end component;

component counter1 is
    	port(
    	    clk, reset, ctrl: in  STD_LOGIC;
			R: out std_logic;
			Q: out unsigned (9 downto 0)
			);
end component;

component GameScreen is
    port ( row : in  unsigned(9 downto 0);
           column : in  unsigned(9 downto 0);
           r : out  std_logic_vector(7 downto 0);
           g : out  std_logic_vector(7 downto 0);
           b : out  std_logic_vector(7 downto 0);
           moveCube: std_logic;
           settings: std_logic_vector(7 downto 0);
           shotTracker: std_logic_vector(14 downto 0);
           shotsTaken: unsigned(3 downto 0);
           control: in std_logic_vector(4 downto 0));
end component;


component blankControl is
 Port ( clk, reset_n, v_or_h: in  STD_LOGIC;
            screenLocation: in unsigned(9 downto 0);
			sync: out std_logic;
			blank: out std_logic);
end component;

signal RollOver1, frameDrawn: STD_logic;
signal rowCounter: unsigned (9 downto 0);
signal columnCounter: unsigned (9 downto 0);
signal v_blank, h_blank: std_logic;


begin

horizontal_counter: counter PORT MAP(clk => clk,
                         ctrl => '1',
                         reset => reset_n,
                         R => RollOver1,
                         Q => rowCounter);
         

vertical_counter: counter1 PORT MAP(clk => clk,
                         reset => reset_n,
                         ctrl => RollOver1,
                         R => frameDrawn,
                         Q => columnCounter);                       
                   
                   


GameView: GameScreen PORT MAP(row => rowCounter,
                               column => columnCounter,
                               r => r,
                               g => g,
                               b => b,
                               moveCube => frameDrawn,
                               settings => settings,
                               shotTracker => shotTracker,
                               shotsTaken => shotsTaken,
                               control => control);
                               
blank_sync_vertical: blankControl PORT MAP( clk => clk,
                                            reset_n => reset_n,
                                            v_or_h => '1',
                                            screenLocation => columnCounter,
                                            sync => v_sync,
                                            blank => v_blank);

blank_sync_horizontal: blankControl PORT MAP(clk => clk,
                                            reset_n => reset_n,
                                            v_or_h => '0',
                                            screenLocation => rowCounter,
                                            sync => h_sync,
                                            blank => h_blank);
                        
blank <= h_blank or v_blank;                   


end structure;
