--------------------------------------------------------------------------------
-- Name:	Jacob Fox
-- Date:	Spring 2020
-- Course: 	CSCE 436
-- File: 	counter1.vhdl
-- HW:		LAB 1 Counter
-- Purp:	3 bit counter that counts to 4 and then rolls over
--
-- Doc:	Basic design came from lecture files
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;		
use IEEE.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;


entity counter1 is
	Port(	clk, reset, ctrl: in  STD_LOGIC;
			R: out std_logic;
			Q: out unsigned (9 downto 0));
end counter1;

architecture behavior of counter1 is
	signal processQ: unsigned (9 downto 0):= "0000000000";

begin
	
	process(clk)
	begin
		if (rising_edge(clk)) then
			if (reset = '0') then
				processQ <= (others => '0');
			elsif ((processQ < 524) and(ctrl = '1')) then
				processQ <= processQ + 1;
			elsif ((processQ = 524) and (ctrl = '1')) then
				processQ <= (others => '0');
			end if;
		end if;
	end process;
 
	R  <= '1' when (processQ = 524) else '0';
	Q <= processQ;
	
end behavior;
