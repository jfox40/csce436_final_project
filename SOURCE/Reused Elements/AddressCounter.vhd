---------------------------------------------------------------------------------
-- Name:	Jacob Fox
-- Date:	Spring 2020
-- Course: 	CSCE 436
-- File: 	The_Box_dp.vhdl
-- HW:		Final Project
-- Purp:	Counter for incrementing over RAM audio array
--
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity AddressCounter is
  Port (clk, reset: in std_logic;
        cw: in std_logic_vector(1 downto 0);
        count: out std_logic_vector(9 downto 0));
end AddressCounter;

architecture Behavioral of AddressCounter is
    
signal tempCount: unsigned(9 downto 0);

begin
process(clk)
    begin
        if rising_edge(clk) then 
            if (cw = "01" or tempCount /= "0000000000") then 
               tempCount <= tempCount + 1;
            elsif cw = "11" then 
               tempCount <= tempCount;
            elsif cw = "00" then 
               tempCount <= "0000000000";
            end if;
        end if;
     end process;
     
     count <= std_logic_vector(tempCount);
end Behavioral;
