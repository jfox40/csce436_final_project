----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/29/2020 02:35:16 PM
-- Design Name: 
-- Module Name: blankControl - structure
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity blankControl is
 Port ( clk, reset_n, v_or_h: in  STD_LOGIC;
            screenLocation: in unsigned (9 downto 0);
			sync: out std_logic;
			blank: out std_logic);
end blankControl;

architecture structure of blankControl is

signal pSync, pBlank: std_logic;


begin

process(clk)
	begin
		if (rising_edge(clk)) then
			if (reset_n = '0') then
				pBlank <= '1';
				pSync <= '0';
				
			--CHECK IF FOR VERTICAL 
			elsif (v_or_h = '1') then
				--PSYNC IF's
			     if((screenLocation > x"1E9") and (screenLocation < x"1EC")) then 
			         pSync <= '0';
			     else
			         pSync <= '1';
			     end if;
			     
			     --PBLANK IF'S
			     if((screenLocation >= x"000") and (screenLocation < x"1E0")) then 
			         pBlank <= '0';
			     else
			         pBlank <= '1';
			     end if;
			     
			--CHECK IF FOR HORIZONTAL
			elsif (v_or_h = '0') then 
			     --PSYNC IF's
			     if((screenLocation > x"28F") and (screenLocation < x"2F0")) then 
			         pSync <= '0';
			     else
			         pSync <= '1';
			     end if;
			     
			     --PBLANK IF'S
			     if((screenLocation >= x"000") and (screenLocation < x"280")) then 
			         pBlank <= '0';
			     else
			         pBlank <= '1';
			     end if;
			     
			end if;
		end if;
	end process;
	
	sync <= pSync;
	blank <= pBlank;

end structure;
