---------------------------------------------------------------------------------
-- Name:	Jacob Fox
-- Date:	Spring 2020
-- Course: 	CSCE 436
-- File: 	Cube_cu.vhdl
-- HW:		Final Project
-- Purp:	Control cube direction and pause cube movement after reading NES controller
--
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity Cube_cu is
  Port (clk: in std_logic;
        reset_n: in std_logic;
        controlInput: in std_logic_vector(4 downto 0);
        cw: out std_logic_vector(2 downto 0));
end Cube_cu;

architecture Behavioral of Cube_cu is

signal direction: std_logic_vector(1 downto 0);
signal pause: std_logic;
signal prevInput: std_logic_vector(4 downto 0);

begin

    process(controlInput)
        begin 
            if(controlInput /= prevInput) then
                prevInput <= controlInput;
                if(controlInput(4) = '0') then 
                    direction <= "00";
                    pause <= '0';
                elsif(controlInput(3) = '0') then 
                    direction <= "01";
                    pause <= '0';
                elsif(controlInput(1) = '0') then 
                    direction <= "11";
                    pause <= '0';
                elsif(controlInput(2) = '0') then 
                    direction <= "10";
                    pause <= '0'; 
                elsif(controlInput(0) = '0') then 
                    pause <= '1'; 
                end if;                
            end if;
         end process;
         
    cw <= pause & direction;

end Behavioral;
