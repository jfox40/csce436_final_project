---------------------------------------------------------------------------------
-- Name:	Jacob Fox
-- Date:	Spring 2020
-- Course: 	CSCE 436
-- File: 	DisplayManager.vhdl
-- HW:		Final Project
-- Purp:	Sends data onward to the video control unit
--
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.theBoxParts.all;

entity DisplayManager is
  Port (clk: in std_logic;
        reset_n: in std_logic;
        tmds: out std_logic_vector(3 downto 0);
        tmdsb: out std_logic_vector(3 downto 0);
        lg_cw: in std_logic_vector(2 downto 0);
        c_cw: in std_logic_vector(2 downto 0);
        set_cw: in std_logic_vector(7 downto 0);
        shotTracker: std_logic_vector(14 downto 0);
        shotsTaken: unsigned(3 downto 0));
end DisplayManager;

architecture Behavioral of DisplayManager is

signal videoControlwords: std_logic_vector(4 downto 0);

begin

VideoControl: Video PORT MAP(clk => clk,
                       reset_n => reset_n,
                       tmds => tmds,
                       tmdsb => tmdsb,
                       settings => set_cw,
                       shotTracker => shotTracker,
                       shotsTaken => shotsTaken,
                       control => videoControlwords);
                       
videoControlwords <= c_cw & lg_cw(1 downto 0);

end Behavioral;
