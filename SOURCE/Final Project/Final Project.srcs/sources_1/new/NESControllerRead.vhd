---------------------------------------------------------------------------------
-- Name:	Jacob Fox
-- Date:	Spring 2020
-- Course: 	CSCE 436
-- File: 	NESControllerRead.vhdl
-- HW:		Final Project
-- Purp:	Top Level entity linking control units and datapath
--
-- Doc:	Used https://www.digikey.com/en/maker/projects/make-an-nes-controller-interface-for-arduino-uno/6d21560a571b490caa2642e87852dacb to 
--      understand how to interface with controller
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity NESControllerRead is
  Port (reset_n: in std_logic;
        dataIn: in std_logic;
        dataOut: out std_logic_vector(7 downto 0);
        nesClk: in std_logic;
        latch: out std_logic);
end NESControllerRead;

architecture Behavioral of NESControllerRead is

type state_type is (latchHigh, latchLow, readbit0, readbit1, readbit2, readbit3, readbit4, readbit5, readbit6, readbit7);
signal currentState: state_type;
signal readData: std_logic_vector(7 downto 0);

begin

process(nesClk)
     
    begin
        --On clock read data, at end of reading all data, set latch and read new data
        if(rising_edge(nesClk)) then
            if(reset_n = '0') then 
               readData <= "00000000";
               currentState <= latchHigh; 
            elsif(currentState = readbit0) then 
               readData(0) <= dataIn;
               currentState <= readbit1;
            elsif(currentState = readbit1) then 
               readData(1) <= dataIn;
               currentState <= readbit2; 
            elsif(currentState = readbit2) then 
               readData(2) <= dataIn;
               currentState <= readbit3; 
            elsif(currentState = readbit3) then 
               readData(3) <= dataIn;
               currentState <= readbit4; 
            elsif(currentState = readbit4) then 
               readData(4) <= dataIn;
               currentState <= readbit5; 
            elsif(currentState = readbit5) then 
               readData(5) <= dataIn;
               currentState <= readbit6; 
            elsif(currentState = readbit6) then 
               readData(6) <= dataIn;
               currentState <= readbit7;
            elsif(currentState = readbit7) then 
               readData(7) <= dataIn;
               currentState <= latchHigh; 
            elsif(currentState = latchHigh) then
                currentState <= latchLow;
            elsif(currentState = latchLow) then 
                currentState <= readbit0;
            end if;
          end if; 
       end process;
            
       latch <= '1' when currentState <= latchHigh else '0';
       dataOut <= readData;           


end Behavioral;
