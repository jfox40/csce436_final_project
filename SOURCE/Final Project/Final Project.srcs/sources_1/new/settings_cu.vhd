----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/10/2020 01:28:13 PM
-- Design Name: 
-- Module Name: settings_cu - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Settings_cu is
  Port (clk: in std_logic;
        reset_n: in std_logic;
        settingData: in std_logic_vector(7 downto 0);
        cw: out std_logic_vector(7 downto 0));
end Settings_cu;

architecture Behavioral of Settings_cu is

begin

cw <= settingData;

end Behavioral;
