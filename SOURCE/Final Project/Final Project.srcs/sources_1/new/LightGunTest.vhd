---------------------------------------------------------------------------------
-- Name:	Jacob Fox
-- Date:	Spring 2020
-- Course: 	CSCE 436
-- File: 	LightGunTest.vhdl
-- HW:		Final Project
-- Purp:	Reads data from modded NES light gun 
--
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Lightgun_cu is
  Port (clk, reset_n: in std_logic;
        playerSelect: in std_logic;
        gunData: in std_logic_vector(2 downto 0);
        P1Hit, P1Miss: out std_logic_vector(3 downto 0);
        cw: out std_logic_vector(2 downto 0);
		shotTracker: out std_logic_vector(14 downto 0);
        shotsTaken: out unsigned(3 downto 0);
        gunClock: in std_logic;
        displayReady: out std_logic;
        matchLength: in std_logic_vector(1 downto 0));
end Lightgun_cu;
  

architecture Behavioral of Lightgun_cu is
 
type state_type is (ResetState, TriggerWait, HitWait, Hit, Miss, displayDone, roundOver);
signal currentState: state_type;


signal triggerPull, triggerHit: std_logic;
signal P1hitCount, P1missCount, prevRoundData: unsigned(3 downto 0);
signal count: unsigned(27 downto 0);
signal shotTrackIndex: integer := 0;
signal endCondition: unsigned(3 downto 0);


begin

--Assign trigger and hit data to signals
triggerPull <= gunData(0);
triggerHit <= gunData(1);

--Set game end conditions from switches
endCondition <= "0101" when matchLength = "00" else
                "1010" when matchLength = "01" else
                "1111" when matchLength = "11" else "0101";

process(gunClock)
    begin 
        if(rising_edge(gunClock)) then
            --Prehit
            if(reset_n = '0') then 
                currentState <= ResetState;
                P1hitCount <= "0000";
                P1missCount <= "0000";
            elsif(currentState = ResetState) then 
                currentState <= TriggerWait;
            elsif(currentState = TriggerWait and triggerPull = '1') then 
                currentState <= HitWait;
            elsif(currentState = HitWait and count < 2000000) then 
                count <= count + 1;
            elsif(currentState = HitWait and count = 2000000) then 
                count <= (others => '0');
                currentState <= displayDone;
                
            --Read Hit Data    
            elsif(currentState = displayDone and triggerHit = '1') then 
                shotTracker(shotTrackIndex) <= '1';
                count <= (others => '0');
                currentState <= Hit;
            elsif(currentState = displayDone and triggerHit = '0' and count < 4000000 ) then 
                count <= count + 1;
                currentState <= displayDone;  
            elsif(currentState = displayDone and triggerHit = '0' and count = 4000000 ) then 
                count <= (others => '0');
                shotTracker(shotTrackIndex) <= '0';
                currentState <= Miss; 
                
            --After Hit    
            elsif(currentState = Hit and triggerPull = '0') then 
                if(shotTrackIndex < endCondition) then 
                    P1hitCount <= P1hitCount + 1;
                    shotTrackIndex <= shotTrackIndex + 1;
                    currentState <= TriggerWait;
                else
                    currentState <= roundOver;
                end if;

            elsif(currentState = Miss and triggerPull = '0') then 
                if(shotTrackIndex < endCondition) then 
                    P1missCount <= P1missCount + 1;
                    shotTrackIndex <= shotTrackIndex + 1;
                    currentState <= TriggerWait;
                else
                    currentState <= roundOver;            
                end if; 
            elsif(currentState = roundOver) then 
                prevRoundData <= P1hitCount;
                currentState <= TriggerWait;
                shotTrackIndex <= 0;
                P1hitCount <= "0000"; 
                P1missCount <= "0000";
            else    
                currentState <= currentState;
            end if;
           end if;
    end process;

displayReady <= '1' when currentState = displayDone else '0';
P1Hit <= std_logic_vector(P1hitCount);
P1Miss <= std_logic_vector(P1missCount) when playerSelect = '0' else std_logic_vector(prevRoundData);
cw(1 downto 0) <= "10" when currentState = HitWait or currentState = displayDone else "00";
cw(2) <= '1' when currentState = HitWait else '0';
shotsTaken <= to_unsigned(shotTrackIndex, 4);

end Behavioral;
