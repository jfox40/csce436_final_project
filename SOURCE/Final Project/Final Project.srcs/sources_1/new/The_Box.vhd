---------------------------------------------------------------------------------
-- Name:	Jacob Fox
-- Date:	Spring 2020
-- Course: 	CSCE 436
-- File: 	The_Box.vhdl
-- HW:		Final Project
-- Purp:	Top Level entity linking control units and datapath
--
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.theBoxParts.all;

entity The_Box is
  Port ( clk : in  STD_LOGIC;
         reset_n : in  STD_LOGIC;
		 ac_mclk : out STD_LOGIC;
		 ac_adc_sdata : in STD_LOGIC;
		 ac_dac_sdata : out STD_LOGIC;
		 ac_bclk : out STD_LOGIC;
		 ac_lrclk : out STD_LOGIC;
         scl : inout STD_LOGIC;
         sda : inout STD_LOGIC;
		 tmds : out  STD_LOGIC_VECTOR (3 downto 0);
         tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
         NESController: in std_logic;
         NESlatch: out std_logic;
         nesClkOut: out std_logic;
         hitData: in std_logic;
         displayReady: out std_logic;
         triggerPull: in std_logic;
         led: out std_logic_vector(7 downto 0);
         sw: in std_logic_vector(7 downto 0));
end The_Box;

architecture Behavioral of The_Box is

--16 and 10Mhz clocks for NES light gun and NES controller
component clk_wiz_2 is
    Port (
        clk_in1 : in STD_LOGIC;
        clk_out1 : out STD_LOGIC;
        clk_out2 : out STD_LOGIC;
        resetn : in STD_LOGIC);
end component;

--Light Gun Signals 
signal ledScore1, ledScore2: std_logic_vector(3 downto 0);
signal lg_cw, gunData: std_logic_vector(2 downto 0);
signal shotTracker: std_logic_vector(14 downto 0);
signal shotsTaken: unsigned(3 downto 0);
signal gunClk: std_logic;

--Settings Signals 
signal set_cw: std_logic_vector(7 downto 0);

--Cube Drive signals 
signal controlInput: std_logic_vector( 7 downto 0);
signal c_cw: std_logic_vector(2 downto 0);
signal nesClk: std_logic;
signal latch: std_logic;

begin

--16 and 10Mhz clocks for NES light gun and NES controller
nesControllerClock: clk_wiz_2
	Port Map (
		clk_in1 => clk,
		clk_out1 => gunClk, 
		clk_out2 => nesClk,
		resetn => reset_n); 

datapath: The_Box_dp PORT MAP(clk => clk,
                              reset_n => reset_n,
                              ac_mclk => ac_mclk,
                              ac_adc_sdata => ac_adc_sdata,
                              ac_dac_sdata => ac_dac_sdata,
                              ac_bclk => ac_bclk,
                              ac_lrclk => ac_lrclk,
                              scl => scl,
                              sda => sda,
                              tmds => tmds,
                              tmdsb => tmdsb,
                              lg_cw => lg_cw,
                              c_cw => c_cw,
                              set_cw => set_cw,
                              shotTracker => shotTracker,
                              shotsTaken => shotsTaken);

lightgunControl: Lightgun_cu PORT MAP( clk => clk,
                                       reset_n => reset_n,
                                       gunData => gunData,
                                       playerSelect => sw(7),
                                       P1Hit => ledScore1,
                                       P1Miss => ledScore2,
                                       cw => lg_cw,
                                       shotTracker => shotTracker,
                                       shotsTaken => shotsTaken,
                                       gunClock => gunClk, 
                                       displayReady => displayReady,
                                       matchLength => sw(5 downto 4));

cubeControl: Cube_cu PORT MAP(clk => clk,
                              reset_n => reset_n,
                              controlInput => controlInput(7 downto 3),
                              cw => c_cw
                              );

controllerRead: NESControllerRead PORT MAP(reset_n => reset_n,
                                           dataIn => NESController,
                                           dataOut => controlInput,
                                           nesClk => nesClk,
                                           latch => nesLatch);

--Out Clock for nes controller 
nesClkOut <= nesClk;

--Data in from gun
gunData <= '0' & hitData & triggerPull;

--Switches to settings control
set_cw <= sw;

--Test LED's
led(7 downto 4) <= ledScore1;
led(3 downto 0) <= ledScore2;

end Behavioral;
