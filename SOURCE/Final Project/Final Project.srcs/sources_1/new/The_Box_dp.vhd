---------------------------------------------------------------------------------
-- Name:	Jacob Fox
-- Date:	Spring 2020
-- Course: 	CSCE 436
-- File: 	The_Box_dp.vhdl
-- HW:		Final Project
-- Purp:	Controls flow of data outside control unit
--
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.theBoxParts.all;


entity The_Box_dp is
  Port (clk: in std_logic;
        reset_n: in std_logic;
        ac_mclk : out STD_LOGIC;
		ac_adc_sdata : in STD_LOGIC;
		ac_dac_sdata : out STD_LOGIC;
		ac_bclk : out STD_LOGIC;
		ac_lrclk : out STD_LOGIC;
        scl : inout STD_LOGIC;
        sda : inout STD_LOGIC;
		tmds : out  STD_LOGIC_VECTOR (3 downto 0);
        tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
        lg_cw: in std_logic_vector(2 downto 0);
        c_cw: in std_logic_vector(2 downto 0);
        set_cw: in std_logic_vector(7 downto 0);
        shotTracker: std_logic_vector(14 downto 0);
        shotsTaken: unsigned(3 downto 0));
end The_Box_dp;

architecture Behavioral of The_Box_dp is

--Audio Signals 
signal effectSelection: std_logic_vector(1 downto 0):= "00";

begin

--Play audio if gun fired 
effectSelection <= "01" when lg_cw(2) = '1' else "00";

Display: DisplayManager PORT MAP( clk => clk,
                                  reset_n => reset_n,
                                  tmds => tmds,
                                  tmdsb => tmdsb,
                                  lg_cw => lg_cw,
                                  c_cw => c_cw,
                                  set_cw => set_cw,
                                  shotTracker => shotTracker,
                                  shotsTaken => shotsTaken);

AudioControl: Audio_Control PORT MAP( clk => clk,
                                      reset_n => reset_n,
                                      ac_mclk => ac_mclk,
                                      ac_adc_sdata => ac_adc_sdata,
                                      ac_dac_sdata => ac_dac_sdata,
                                      ac_bclk => ac_bclk,
                                      ac_lrclk => ac_lrclk,
                                      scl => scl,
                                      sda => sda,
                                      effectSelection => effectSelection);


end Behavioral;
