---------------------------------------------------------------------------------
-- Name:	Jacob Fox
-- Date:	Spring 2020
-- Course: 	CSCE 436
-- File: 	Audio_Control.vhdl
-- HW:		Final Project
-- Purp:	Plays predefined audio wave over audio_codec
--
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
library UNIMACRO;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use UNIMACRO.vcomponents.all;
use work.theBoxParts.all;


entity Audio_Control is
  Port (clk: in std_logic;
        reset_n: in std_logic;
        ac_mclk : out STD_LOGIC;
		ac_adc_sdata : in STD_LOGIC;
		ac_dac_sdata : out STD_LOGIC;
		ac_bclk : out STD_LOGIC;
		ac_lrclk : out STD_LOGIC;
        scl : inout STD_LOGIC;
        sda : inout STD_LOGIC;
        effectSelection: in std_logic_vector(1 downto 0));
end Audio_Control;

architecture Behavioral of Audio_Control is

signal ready: std_logic;
signal L_bus_in, R_bus_in, L_bus_out, R_bus_out: std_logic_vector(17 downto 0); 
signal AudioData: std_logic_vector(15 downto 0);
signal RDADDR: std_logic_vector(9 downto 0);

begin

             ShotNoise: BRAM_SDP_MACRO
	           generic map (
		          BRAM_SIZE => "18Kb", 			-- Target BRAM, "18Kb" or "36Kb"
		          DEVICE => "7SERIES", 			-- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
		          DO_REG => 0, 					-- Optional output register disabled
		          INIT => X"0000000000000000",	-- Initial values on output port
		          INIT_FILE => "NONE",			-- Not sure how to initialize the RAM from a file
		          WRITE_WIDTH => 16, 				-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
		          READ_WIDTH => 16, 				-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
		          SIM_COLLISION_CHECK => "NONE", 			-- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
		          SRVAL => X"000000000000000000",	-- Set/Reset value for port output
                    INIT_00 => X"0005000500040003000300020002000100010001000000000000000000000000",
                    INIT_01 => X"001600150014001200110010000F000E000C000B000A00090008000800070006",
                    INIT_02 => X"00320030002E002C002A002800270025002300210020001E001C001B00190018",
                    INIT_03 => X"0058005500530050004E004B0049004600440042003F003D003B003900360034",
                    INIT_04 => X"008600830080007D007A007700740071006E006B0068006500630060005D005A",
                    INIT_05 => X"00BB00B700B400B100AD00AA00A600A300A0009C009900960093008F008C0089",
                    INIT_06 => X"00F500F100ED00EA00E600E200DF00DB00D700D400D000CD00C900C500C200BE",
                    INIT_07 => X"0131012D012901260122011E011A01160113010F010B01070104010000FC00F8",
                    INIT_08 => X"016E016A01660162015F015B01570153014F014C014801440140013C01390135",
                    INIT_09 => X"01A701A401A0019D019901960192018F018B018701840180017C017901750171",
                    INIT_0A => X"01DC01D901D601D301CF01CC01C901C601C301BF01BC01B901B501B201AE01AB",
                    INIT_0B => X"020802060203020101FE01FB01F901F601F301F001EE01EB01E801E501E201DF",
                    INIT_0C => X"022902270225022402220220021E021C021A0218021602140211020F020D020A",
                    INIT_0D => X"023B023A0239023902380237023602350234023302310230022F022D022C022A",
                    INIT_0E => X"023C023C023C023D023D023D023D023E023E023D023D023D023D023C023C023B",
                    INIT_0F => X"0228022A022C022D022F023002320233023402360237023802390239023A023B",
                    INIT_10 => X"01FD020102040207020A020D0210021302160218021B021D021F022202240226",
                    INIT_11 => X"01B901BE01C301C801CD01D101D601DA01DE01E301E701EB01EF01F201F601FA",
                    INIT_12 => X"015901600166016D0174017A01800186018C01920198019E01A401A901AE01B4",
                    INIT_13 => X"00DB00E400EC00F500FD0105010E0116011E0125012D0135013C0143014B0152",
                    INIT_14 => X"003D00480053005D00680072007C00860090009A00A300AD00B600C000C900D2",
                    INIT_15 => X"FF80FF8DFF9AFFA6FFB3FFBFFFCBFFD7FFE3FFEFFFFB00050011001C00270032",
                    INIT_16 => X"FEA0FEAFFEBEFECDFEDCFEEAFEF8FF06FF15FF22FF30FF3EFF4BFF59FF66FF73",
                    INIT_17 => X"FD9FFDB0FDC1FDD2FDE3FDF3FE04FE14FE24FE34FE44FE54FE63FE73FE82FE91",
                    INIT_18 => X"FC7CFC8FFCA2FCB5FCC8FCDAFCEDFCFFFD12FD24FD36FD48FD59FD6BFD7CFD8E",
                    INIT_19 => X"FB38FB4DFB62FB77FB8CFBA0FBB5FBC9FBDEFBF2FC06FC1AFC2EFC42FC55FC69",
                    INIT_1A => X"F9D4F9EBFA02FA19FA2FFA46FA5DFA73FA89FAA0FAB6FACCFAE2FAF7FB0DFB22",
                    INIT_1B => X"F851F86AF883F89CF8B5F8CDF8E6F8FEF916F92EF946F95EF976F98DF9A5F9BC",
                    INIT_1C => X"F6B3F6CEF6E8F703F71DF737F752F76CF786F79FF7B9F7D3F7ECF806F81FF838",
                    INIT_1D => X"F4FCF518F534F550F56CF587F5A3F5BFF5DAF5F6F611F62CF647F662F67DF698",
                    INIT_1E => X"F32EF34CF369F386F3A3F3C1F3DEF3FBF417F434F451F46EF48AF4A7F4C3F4DF",
                    INIT_1F => X"F14FF16DF18BF1AAF1C8F1E6F204F222F240F25EF27CF29AF2B8F2D6F2F3F311",
                    INIT_20 => X"EF61EF80EF9FEFBEEFDDEFFCF01BF03AF059F078F097F0B6F0D4F0F3F111F130",
                    INIT_21 => X"ED6AED89EDA9EDC8EDE8EE08EE27EE47EE66EE85EEA5EEC4EEE4EF03EF22EF42",
                    INIT_22 => X"EB6EEB8EEBADEBCDEBEDEC0DEC2CEC4CEC6CEC8CECABECCBECEBED0BED2AED4A",
                    INIT_23 => X"E973E993E9B2E9D2E9F1EA11EA31EA50EA70EA90EAAFEACFEAEFEB0FEB2EEB4E",
                    INIT_24 => X"E77FE79EE7BDE7DCE7FBE81AE839E858E878E897E8B6E8D6E8F5E915E934E953",
                    INIT_25 => X"E597E5B5E5D3E5F1E60FE62DE64CE66AE689E6A7E6C6E6E4E703E722E741E760",
                    INIT_26 => X"E3C1E3DDE3FAE417E434E451E46EE48CE4A9E4C6E4E4E501E51FE53DE55BE579",
                    INIT_27 => X"E203E21EE239E255E270E28CE2A7E2C3E2DFE2FAE317E333E34FE36BE388E3A4",
                    INIT_28 => X"E064E07DE096E0B0E0C9E0E2E0FCE116E130E14AE164E17EE198E1B3E1CEE1E8",
                    INIT_29 => X"DEEADF01DF17DF2EDF45DF5CDF73DF8BDFA2DFBADFD2DFEAE002E01BE033E04C",
                    INIT_2A => X"DD9ADDAEDDC2DDD6DDEADDFEDE13DE28DE3DDE52DE67DE7CDE92DEA8DEBEDED4",
                    INIT_2B => X"DC7BDC8CDC9CDCADDCBEDCCFDCE1DCF2DD04DD16DD29DD3BDD4EDD61DD74DD87",
                    INIT_2C => X"DB92DB9FDBACDBB9DBC7DBD5DBE3DBF1DBFFDC0EDC1DDC2CDC3BDC4BDC5BDC6B",
                    INIT_2D => X"DAE3DAECDAF6DAFFDB09DB13DB1EDB28DB33DB3EDB49DB55DB60DB6CDB79DB85",
                    INIT_2E => X"DA75DA7ADA7FDA85DA8ADA90DA97DA9DDAA4DAABDAB2DABADAC2DACADAD2DADB",
                    INIT_2F => X"DA4BDA4CDA4CDA4DDA4FDA51DA53DA55DA57DA5ADA5DDA60DA64DA68DA6CDA70",
                    INIT_30 => X"DA69DA65DA61DA5EDA5BDA58DA55DA53DA51DA4FDA4EDA4CDA4CDA4BDA4BDA4B",
                    INIT_31 => X"DAD2DACADAC1DAB9DAB1DAA9DAA2DA9BDA94DA8EDA88DA82DA7CDA77DA72DA6D",
                    INIT_32 => X"DB8ADB7CDB6FDB61DB55DB48DB3CDB30DB24DB19DB0EDB03DAF9DAEFDAE5DADC",
                    INIT_33 => X"DC91DC7EDC6CDC59DC48DC36DC25DC14DC03DBF3DBE3DBD3DBC4DBB5DBA6DB98",
                    INIT_34 => X"DDE9DDD1DDBADDA2DD8CDD75DD5FDD49DD33DD1EDD09DCF4DCDFDCCBDCB8DCA4",
                    INIT_35 => X"DF93DF76DF59DF3DDF21DF05DEEADECFDEB4DE99DE7FDE66DE4CDE33DE1ADE01",
                    INIT_36 => X"E18EE16CE14AE129E108E0E7E0C6E0A6E086E067E048E029E00ADFECDFCEDFB0",
                    INIT_37 => X"E3D9E3B2E38BE365E33FE319E2F3E2CEE2A9E285E261E23DE219E1F6E1D3E1B0",
                    INIT_38 => X"E672E646E61AE5EFE5C4E59AE56FE545E51CE4F2E4C9E4A0E478E44FE428E400",
                    INIT_39 => X"E956E925E8F5E8C5E896E866E837E809E7DAE7ACE77EE751E724E6F7E6CAE69E",
                    INIT_3A => X"EC81EC4DEC18EBE4EBB0EB7CEB49EB16EAE3EAB0EA7EEA4CEA1AE9E8E9B7E986",
                    INIT_3B => X"EFF0EFB8EF7FEF47EF0FEED7EE9FEE68EE31EDFAEDC3ED8DED57ED21ECECECB6",
                    INIT_3C => X"F39EF361F325F2E9F2ADF271F236F1FBF1C0F185F14AF110F0D6F09CF063F029",
                    INIT_3D => X"F783F743F703F6C4F685F646F607F5C8F58AF54BF50DF4D0F492F454F417F3DA",
                    INIT_3E => X"FB9AFB58FB15FAD2FA90FA4EFA0CF9CAF989F947F906F8C5F884F844F803F7C3",
                    INIT_3F => X"FD2EFCEBFCA7FC64FC20FBDDFB9AFB58FB15FAD2FA90FA4EFA0CF9CAF989F947")
	           port map (
		          DO => AudioData,				-- Output read data port, width defined by READ_WIDTH parameter
		          RDADDR => RDADDR,			-- Input address, width defined by port depth
		          RDCLK => clk,	 				-- 1-bit input clock
		          RST => '0',					-- active high reset
		          DI => "0100000111110100",
		          RDEN => '1',                    -- read enable 					
		          REGCE => '1',					-- 1-bit input read output register enable - ignored
		          WE => "11",			-- since RAM is byte read, this determines high or low byte
		          WRADDR => "0000000000",
		          WRCLK => clk,		 			-- 1-bit input write clock
		          WREN => '0');					-- 1-bit input write port enable 

    AddressCount: AddressCounter PORT MAP(clk => ready,
                                          reset => reset_n,
                                          cw => effectSelection,
                                          count => RDADDR);
    
	Audio_Codec: Audio_Codec_Wrapper Port Map( 
	    clk => clk,
        reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
        ready => ready,
        L_bus_in => L_bus_out,
        R_bus_in => R_bus_out,
        L_bus_out => L_bus_in,
        R_bus_out => R_bus_in,
        scl => scl,
        sda => sda );
        
        L_bus_out <= AudioData & "00";
        R_bus_out <= AudioData & "00";

end Behavioral;
