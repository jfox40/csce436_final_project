const int lightSense = A0;
const int hitData = 6;
const int triggerOut = 7;
const int displayReady = 5;

void setup() {
  Serial.begin(9600);
  pinMode(lightSense,INPUT);
  pinMode(hitData, OUTPUT);
  pinMode(displayReady, INPUT);
  pinMode(triggerOut, OUTPUT); 
  attachInterrupt(digitalPinToInterrupt(3), sendTrigger, FALLING);
}

void loop() {
  if(digitalRead(displayReady)){
     int value = analogRead(lightSense);
     if(value > 100){
          digitalWrite(hitData, HIGH);
     }
  }
  else{
    digitalWrite(hitData, LOW);
  }  
}

void sendTrigger(){
    digitalWrite(triggerOut, HIGH);
    digitalWrite(triggerOut, LOW);
}
